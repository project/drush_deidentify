<?php

/**
 * @file
 *  provides hook info for drush_deidentify
 */

/**
 * Run on a database after imported.
 *
 * @param array $tables
 *
 * @return void
 */
function hook_drush_deidentify_data_clean(array &$tables) {
  // Here alter or provide extra tables to clean.
  /* // Add new tables like this
  $tables['table_name'] => [
    'colum_to_clean_1',
    'colum_to_clean_2'
  ];
   */
}

/**
 * Run on database during export.
 *
 * @param array $tables
 *
 * @return void
 */
function hook_drush_deidentify_export(array &$tables) {
  // Here alter or provide extra tables to clean.
  /*
  $extra_tables = [
    'watchdog',
    'webform_submission_data'
  ];
  $tables = array_merge($tables, $extra_tables);
   */
}

/**
 * Granular control over data set.
 *
 * @param $value
 *   the current altered value if any.
 * @param $type
 *   The field type.
 * @param $field_name
 *   The database field name.
 * @param $table_name
 *   The datbase table name
 * @param $colum_details
 *   The details about the colum
 * @param $faker
 *   faker object  @see https://fakerphp.github.io/
 * @param $current_db_item
 *   the current database record with values.
 *
 * @return void
 *   should override &$value  if needed.
 */
function hook_drush_deidentify_data_clean_colum_val(&$value, $type, $field_name, $table_name, $colum_details, $faker, $current_db_item) {
  // Here you can change the $value of the field based on the available details. use the passed in $faker or dump($current_db_item) for more info.
  /*
  // Example run  drush  drush_deidentify:db-clean webform_submission_data~value
  if (!empty($current_db_item)) {
    if (property_exists($current_db_item, 'webform_id') && property_exists($current_db_item, 'name')) {
       if ($current_db_item->webform_id === 'contact' && $current_db_item->name == 'email') {
         $value = $faker->safeEmail()
       }
     }
  }
  // Or  a more simple example
  if ($table_name === 'x' && $field_name == 'y') {
    $value = $faker->safeEmail();
  }
   */
}

/**
 * Hook to ensure the cleand table prime key is correct.
 *
 * @param $prime_key_field
 * @param $details
 * @param $table_name
 *
 * @return void
 */
function hook_drush_deidentify_data_clean_force_prime_key(&$prime_key_field, $details, $table_name) {
  // Here you may need to be 100% sure you have the correct prime key for your table
  // You can only alter $prime_key_field value.
}
