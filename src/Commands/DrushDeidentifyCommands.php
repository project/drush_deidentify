<?php

namespace Drupal\drush_deidentify\Commands;

use Drush\Commands\DrushCommands;
use Drush\Commands\sql\SqlCommands;
use Drush\Exceptions\UserAbortException;
use Drush\Sql\SqlBase;
use Consolidation\OutputFormatters\StructuredData\PropertyList;
use Faker\Factory as FakerFactory;

/**
 * Provides Drush commands drush_deidentify:db-export and drush_deidentify:db-clean.
 */
class DrushDeidentifyCommands extends DrushCommands {

    /**
     * Drush\Commands\sql\SqlCommands
     * @var SqlCommands
     */
    protected $sqlCommands;
    protected $database;
    protected $moduleHandler;
    public $faker;

    /**
     * SanitizeCommentsCommands constructor.
     *
     * @param $database
     * @param $moduleHandler
     */
    public function __construct($database, $moduleHandler) {
        $this->database = $database;
        $this->moduleHandler = $moduleHandler;
        $this->sqlCommands = new SqlCommands();
        $this->faker = FakerFactory::create();
    }

    /**
     * Drush Export Database excluding tables.
     *
     * @option omit_tables A comma-separated list of tables to include for structure, but not data.
     * @option result-file Save to a file. The file should be relative to Drupal root.
     * @option gzip Compress the dump using the gzip program which must be in your <info>$PATH</info>.
     * @usage drush_deidentify-db-export
     *   Usage description
     *
     * @command drush_deidentify:db-export
     * @aliases d-export
     */
    public function dbExport($options = ['result-file' => DrushCommands::REQ, 'gzip' => FALSE, 'omit_tables' => '']) {
        if (empty($options)) {
            $this->logger()->error(dt('Missing output file name example: --result-file=dbexport.sql'));
            $this->exitClean();
        }
        if (!empty($options) && empty($options['result-file'])) {
            $this->logger()->error(dt('Missing output file name example:  --result-file=dbexport.sql'));
            $this->exitClean();
        }

        $default_tables = [
            'cache_bootstrap',
            'cachetags',
            'cache_config',
            'cache_container',
            'cache_data',
            'cache_default',
            'cache_discovery',
            'cache_dynamic_page_cache',
            'cache_entity',
            'cache_menu',
            'cache_page',
            'cache_render',
            'cache_toolbar',
        ];

        if (!empty($options) && !empty($options['omit_tables'])) {
            $tables = explode(',', $options['omit_tables']);
            unset($options['omit_tables']);
            if (!empty($tables) && is_array($tables)) {
                $default_tables = array_merge($default_tables, $tables);
            }
        }

        // Offer hook so no need to alter this module.
        $this->moduleHandler->invokeAll('drush_deidentify_export', [&$default_tables]);
        $options['structure-tables-list'] = implode(',', $default_tables);
        $this->logger()->success(dt('Db tables skipped  @skiped', ['@skiped' => $options['structure-tables-list']]));
        $options['skip-tables-key'] = NULL;
        $options['skip-tables-list'] = NULL;
        $options['structure-tables-key'] = NULL;
        $options['tables-key'] = NULL;
        $options['tables-list'] = NULL;

        unset($options['omit_tables']);

        $sql = SqlBase::create($options);
        $return = $sql->dump();
        if ($return === FALSE) {
            throw new \Exception('Unable to dump database. Rerun with --debug to see any error message.');
        }
        // SqlBase::dump() returns null if 'result-file' option is empty.
        if ($return) {
            $this->logger()->success(dt('Database dump saved to !path (relative to drupal root)', ['!path' => $return]));
        }
        return new PropertyList(['path' => $return]);
    }

    /**
     * Database Clean after import.
     *
     * @param string $clean_tables
     *   A comma-separated list of tables to clean eg watchdog~message, watchdog~location.
     *
     * @usage drush_deidentify-db-clean
     *   Example drush_deidentify-db-clean watchdog.
     *
     * @command drush_deidentify:db-clean
     * @aliases d-clean
     */
    public function dbClean(string $clean_tables = '') {
        $tables = [];
        if (!empty($clean_tables)) {
            $tables_to_validate = explode(',', $clean_tables);
            foreach ($tables_to_validate as $item) {
                if (strpos($item, '~') !== FALSE) {
                    // User Supplied Idea to have per colum
                    // "watchdog~message".
                    $broken_string = explode('~', $item);
                    if (count($broken_string) == 2) {
                        $tname = $broken_string[0];
                        $cname = $broken_string[1];
                        $tables[$tname][] = $cname;
                    }
                    else {
                        $this->logger()->error(
                            dt(
                                'Please check the argument @table it does not have required structure eg table~column ', [
                                    '@table' => $item,
                                ]
                            )
                        );
                        $this->exitClean();
                    }
                }
                else {
                    $this->logger()->error(
                        dt(
                            'Please check the argument @table it does not have required structure eg table~column ', [
                                '@table' => $item,
                            ]
                        )
                    );
                    $this->exitClean();
                }
            }
        }

        // Offer hook so no need to alter this module.
        /**
         * @see the modules api file.
         */
        $this->moduleHandler->invokeAll('drush_deidentify_data_clean', [&$tables]);

        if (empty($tables)) {
            $this->logger()->error(dt('Think you need to add some tables as an argument eg drush_deidentify:db-clean watchdog~message or by hook_drush_deidentify_data_clean'));
            $this->exitClean();
        }

        $this->logger()->notice(dt('Table(s) to clean:  @clean ', ['@clean' => implode(',', array_keys($tables))]));
        if (!$this->getConfig()->simulate() && !$this->io()->confirm(dt('Do you really want to continue?'))) {
            throw new UserAbortException();
        }

        // Get the table details and continue.
        $table_cols_values = [];
        $table_prime_keys = [];
        foreach ($tables as $table_name => $columns) {
            try {
                /**
                 * @todo find out why the normal...
                 * $this->database->query('DESCRIBE {@table}', ['@table' => $table]);
                 * does not work ????????
                 */
                $query_string = "DESCRIBE `" . trim($table_name) . '`';
                $query = $this->database->query($query_string);
                $rows = [];
                while ($result = $query->fetchAssoc()) {
                    $rows[] = $result;
                    if (in_array($result['Field'], $columns)) {
                        $table_cols_values[$table_name][] = $result;
                        $s = $result['Field'];
                        $columns = array_flip($columns);
                        unset($columns[$s]);
                        if (!empty($columns)) {
                            $columns = array_flip($columns);
                        }
                    }
                }

                if (!empty($columns)) {
                    // Computer says no.
                    $this->logger()->error(
                        dt(
                            'Error: these columns are not found in table -> @key  @cols', [
                                '@cols' => json_encode($columns),
                                '@key' => $table_name,
                            ]
                        )
                    );
                    $this->exitClean();
                }

                $prime_key = $this->findPrimeKeyForTable($rows, $table_name);
                if (!empty($prime_key)) {
                    if ($prime_key) {
                        $this->logger->notice(
                            dt(
                                'Prime key for @table set to: @key ', [
                                    '@table' => $table_name,
                                    '@key' => $prime_key,
                                ]
                            )
                        );
                        $table_prime_keys[$table_name] = $prime_key;
                    }
                }
                else {
                    $this->logger()->error(dt('Sorry we can\'t find a prime key for @table you will need to use hook_drush_deidentify_data_clean_force_prime_key', ['@table' => $table_name]));
                    $this->exitClean();
                }
                // @todo should we die here.
            }
            catch (\Exception $e) {
                $this->logger()->error(
                    dt(
                        'Error with argument @table : got @message',
                        [
                            '@table' => $table_name,
                            '@message' => $e->getMessage(),
                        ]
                    )
                );
                $this->exitClean();
            }
        }

        if (!$this->getConfig()->simulate() && !$this->io()->confirm(dt('Please confirm above table prime key(s) if not correct stop here.'))) {
            $this->logger()->notice(dt('use hook_drush_deidentify_data_clean_force_prime_key to set the correct prime.'));
            throw new UserAbortException();
        }

        $this->logger()->notice(dt('Cleaning...'));

        if (!empty($table_cols_values)) {
            $field_values = [];
            foreach ($table_cols_values as $table_name => $cols_value) {
                $sql = "SELECT * FROM {" . $table_name . "}";
                $query = $this->database->query($sql);
                // Here we have all the things.
                $result = $query->fetchAll();
                $num_results = count($result);
                if ($num_results = 0) {
                    // No were not generating new stuff thats devel_gennerate job.
                    continue;
                }
                else {
                    foreach ($result as $item) {
                        $fields_and_values = [];
                        foreach ($cols_value as $details) {
                            $col_name = $details['Field'];
                            if (!in_array($col_name, $this->ignoreFieldNames())) {
                                $fields_and_values[$col_name] = $this->generateFakeValueForField($details['Type'], $col_name, $table_name, $details, $item);
                            }
                        }

                        if (!empty($table_prime_keys[$table_name])) {
                            $prime_key = $table_prime_keys[$table_name];

                            if (!empty($item->{$prime_key}) && !empty($fields_and_values)) {
                                $current_row_prime_key_value = $item->{$prime_key};

                                try {
                                    $query = $this->database->update($table_name)
                                        ->fields($fields_and_values)
                                        ->condition($prime_key, $current_row_prime_key_value);

                                    if (!empty($item)) {
                                        $the_key = array_key_first($fields_and_values);
                                        if (!empty($item->{$the_key})) {
                                            $current_val = $item->{$the_key};
                                            $query->condition($the_key, $current_val);
                                        }
                                    }

                                    $query->execute();
                                }
                                catch (\Exception $exception) {
                                    $this->logger()->error(
                                        dt(
                                            'As this is WORK in progress  please check the method generateFakeValueForField  and --> this  @message', [
                                                '@message' => $exception->getMessage(),
                                            ]
                                        )
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        drupal_flush_all_caches();
        $this->logger()->success(dt('Database has been cleaned and cache cleared'));
    }

    /**
     * todo clean exit.
     */
    public function exitClean() {
        die('Command Exit');
    }

    /**
     * @param  array $details
     *   The result of all DESCRIBE table.
     * @return false|mixed
     */
    public function findPrimeKeyForTable(array $details, $table_name) {
        $prime_key_field = FALSE;
        foreach ($details as $col) {
            if (!empty($col['Key'])) {
                if ($col['Key'] == 'PRI') {
                    if (!in_array(
                        $col['Field'], [
                            'deleted',
                            'delta',
                            'property',
                            // 'name'
                        ]
                    )
                    ) {
                        if ($col['Type']) {
                            // Not using str_contains yet.
                            if (strpos($col['Type'], 'int') !== FALSE || strpos($col['Type'], 'varchar') !== FALSE) {
                                $prime_key_field = $col['Field'];
                            }
                        }
                    }
                }
            }
        }

        // Here ensure you can alter this field if required. like ($prime_key_field = nid) ect.
        $this->moduleHandler->invokeAll('drush_deidentify_data_clean_force_prime_key', [&$prime_key_field, $details, $table_name]);

        return $prime_key_field;
    }

    /**
     * Sets the value dependent on type.
     *
     * @param $type
     * @param $field_name
     * @param $table_name
     * @param $colum_details
     * @param $current_db_item
     *
     * @return int|string
     */
    public function generateFakeValueForField($type, $field_name, $table_name, $colum_details, $current_db_item) {
        $value = '';

        switch ($type) {
            case 'varchar(255)':
                $value = $this->faker->lexify();
                break;

            case 'longtext':
                // SET value here.
                $num = mt_rand(150, 550);
                $value = $this->faker->sentence($num);
                break;

            case 'decimal(14,3)':
            case 'int(11)':
                $value = $this->faker->unixTime();
                break;

            case 'text':
            case 'mediumtext':
                $value = $this->faker->word();
                break;

            default;
                break;
        }

        // Hook here to make granular decisions.
        $this->moduleHandler->invokeAll('drush_deidentify_data_clean_colum_val', [&$value, $type, $field_name, $table_name, $colum_details, $this->faker, $current_db_item]);

        return $value;
    }

    /**
     * Field names to Ignore.
     */
    public function ignoreFieldNames() :array {
        return [
            'revision_id',
            'entity_id',
            'deleted',
            'bundle',
            'langcode',
            'delta',
        ];
    }
}
